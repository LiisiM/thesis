﻿using System;

namespace PersonNames.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {         
            var name = new PersonName();
            Console.Write("First name > ");
            name.GivenName = Console.ReadLine();
            Console.Write("Last name > ");
            name.FamilyName = Console.ReadLine();
            Console.Write("Prefix > ");
            name.Prefix = Console.ReadLine();
            Console.Write("Suffix > ");
            name.Suffix = Console.ReadLine();

            Console.Write("Preferred name > ");
            name.PreferredName = Console.ReadLine();

            Console.Write("Enter format > ");
            var format = Console.ReadLine();
          
            Console.WriteLine("Formatted: " + name.ToString(format));
            Console.ReadLine();

            return;
        }
    }
}
