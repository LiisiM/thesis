﻿using System;
using NUnit.Framework;

namespace PersonNames.Tests
{
    [TestFixture]
    public class FormatterTests
    {
        [Test, TestCaseSource(typeof(TestDataFactory), "NamesForFormatter")]
        public String TestFormat(PersonName name, String format)
        {
            return Formatter.Format(name, format);
        }
    }
}
