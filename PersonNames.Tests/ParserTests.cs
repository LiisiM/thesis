﻿using System;
using NUnit.Framework;

namespace PersonNames.Tests
{
    [TestFixture]
    public class ParserTests
    {
        //private CSharpNameParser.NameParser otherParser = new CSharpNameParser.NameParser();

        [Test, TestCaseSource(typeof(TestDataFactory), "NamesForParser")]
        public String[] TestParse(String nameString)
        {
            var name = Parser.Parse(nameString);
            return new String[] { name.GivenName, name.FamilyName, name.Prefix, name.Suffix };
        }

        //[Test, TestCaseSource(typeof(TestDataFactory), "NamesForOtherCSharpParser")]
        //public String[] TestParseOtherCharpParser(String nameString)
        //{
        //    var name = otherParser.Parse(nameString);
        //    return new String[] { 
        //        name.FirstName.ToUpperInvariant(), 
        //        name.LastName.ToUpperInvariant(), 
        //        name.Salutation.ToUpperInvariant(), 
        //        name.Suffix.ToUpperInvariant()
        //    };
        //}
    }
}
