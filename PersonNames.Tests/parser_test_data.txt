#Simple names
Anna Mets;Anna;Mets;;;General
Jaan Markus Murakas;Jaan Markus;Murakas;;;General
Jana-Liisa Mutt;Jana-Liisa;Mutt;;;General
Ave Murakas-Mutt;Ave;Murakas-Mutt;;;General
Jana-Liisa Mutt-Harakas;Jana-Liisa;Mutt-Harakas;;;General

#Family names with nobility particles
Karl Ernst von Baer;Karl Ernst;von Baer;;;Nobility particle
Johann von der Hagen;Johann;von der Hagen;;;Nobility particle
Johann vom Berg;Johann;vom Berg;;;Nobility particle
Johann von zur M�hlen;Johann;von zur M�hlen;;;Nobility particle
Johann von und zu Urf;Johann;von und zu Urf;;;Nobility particle

#Family names with multiple parts
Johann Meyer zu Selhausen;Johann;Meyer zu Selhausen;;;Multipart FamilyName
Johann Holm und Schwarzwald;Johann;Holm und Schwarzwald;;;Multipart FamilyName
Johann Sankt Goar;Johann;Sankt Goar;;;Multipart FamilyName
Matthew St. John;Matthew;St. John;;;Multipart FamilyName
Johann Schmidt genannt M�ller;Johann;Schmidt genannt M�ller;;;Multipart FamilyName
Adolf Friedrich Graf von Schack;Adolf Friedrich;Graf von Schack;;;Multipart FamilyName
Susan De La Mare;Susan;De La Mare;;;Multipart FamilyName

#Family name with 2 separate names
Helena Bonham Carter;Helena;Bonham Carter;;;Multipart FamilyName

#Suffixes
Benjamin Disraeli, Earl of Beaconsfield;Benjamin;Disraeli;;Earl of Beaconsfield;Suffix
Henry Home, Lord Kames;Henry;Home;;Lord Kames;Suffix

#Prefixes
Dame Anne Susan Potter;Anne Susan;Potter;Dame;;Prefix
Lord Justice Ian Smith;Ian;Smith;Lord Justice;;Prefix

#Prefix and suffix
Sir Thomas Birch, MSc;Thomas;Birch;Sir;MSc;Prefix + Suffix
Sir Thomas Birch MSc;Thomas;Birch;Sir;MSc;Prefix + Suffix
Sir Thomas Birch Jr, PhD;Thomas;Birch;Sir;Jr,PhD;Prefix + Multiple Suffixes
Sir Thomas Birch Jr, PhD, Esq;Thomas;Birch;Sir;Jr,PhD,Esq;Prefix + Multiple Suffixes