﻿using System;
using System.Collections;
using NUnit.Framework;

namespace PersonNames.Tests
{
    public class TestDataFactory
    {
        //format;expected;given;family;prefix;suffix;preferred;category;testname
        public static IEnumerable NamesForFormatter
        {
            get
            {
                var file = System.IO.File.ReadAllLines("format_test_data.txt");
                foreach (var line in file)
                {
                    var parts = line.Split(';');
                    if (String.IsNullOrWhiteSpace(line) || parts.Length != 9 || line.StartsWith("#"))
                    {
                        continue;
                    }
                    var category = (parts[7].Length > 0 ? parts[7] : "GeneralFormat");

                    yield return new TestCaseData(new PersonName
                    {
                        GivenName = parts[2],
                        FamilyName = parts[3],
                        Prefix = parts[4],
                        Suffix = parts[5],
                        PreferredName = parts[6]
                    }, parts[0])
                        .Returns(parts[1])
                        .SetCategory(category)
                        .SetDescription(parts[8])
                        .SetName(String.Format("Format - {0} - {1}", category, parts[8]));
                }
            }
        }

        // fullname;given;family;prefix;suffix;category
        public static IEnumerable NamesForParser
        {
            get
            {
                var file = System.IO.File.ReadAllLines("parser_test_data.txt");
                foreach (var line in file)
                {
                    var parts = line.Split(';');
                    if (String.IsNullOrWhiteSpace(line) || parts.Length != 6 || 
                        line.StartsWith("#"))
                    {
                        continue;
                    }
                    var category = (parts[5].Length > 0 ? parts[5] : "GeneralParser");

                    yield return new TestCaseData(parts[0])
                        .Returns(new String[] { parts[1], parts[2], parts[3], parts[4] })
                        .SetCategory(category)
                        .SetName(String.Format("Parse - {0} - {1}", category, parts[0]));
                }
            }
        }

        // fullname;given;family;prefix;suffix;category
        public static IEnumerable NamesForOtherCSharpParser
        {
            get
            {
                var file = System.IO.File.ReadAllLines("parser_test_data.txt");
                foreach (var line in file)
                {
                    var parts = line.Split(';');
                    if (String.IsNullOrWhiteSpace(line) || parts.Length != 6 || line.StartsWith("#"))
                    {
                        continue;
                    }
                    var category = (parts[5].Length > 0 ? parts[5] : "GeneralParser");

                    yield return new TestCaseData(parts[0])
                        .Returns(new String[] { 
                            parts[1].ToUpperInvariant(), 
                            parts[2].ToUpperInvariant(), 
                            parts[3].ToUpperInvariant(), 
                            parts[4].ToUpperInvariant() 
                        })
                        .SetCategory(category)
                        .SetName(String.Format("Other Parse - {0} - {1}", category, parts[0]));
                }
            }
        }
    }
}
