﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace PersonNames
{
    public static class Formatter
    {
        public static String Format(PersonName name, String formatString)
        {
            // cut format string to parts
            var formatParts = parseFormatString(formatString);

            // loop through parts and format them separately
            // finally replace format part/placeholder in format string
            foreach (var formatPart in formatParts)
            {
                var namePart = formatNamePart(name, formatPart);
                formatString = formatString.Replace(formatPart, namePart);
            }
            return formatString;
        }

        private static IEnumerable<String> parseFormatString(String formatString)
        {
            Regex regex = new Regex(@"(\{.*?\}(?:\*|\d*)?(?:(?<!\})\:(?:\*|\d*))?)", RegexOptions.IgnoreCase);

            return regex.Matches(formatString)
                        .Cast<Match>()
                        .Select(x => x.Value.Trim())
                        .Where(x => !String.IsNullOrEmpty(x));
        }


        private static String formatNamePart(PersonName name, string formatPart)
        {

            // Extract everything between brackets (skip first bracket, take everyhign thats not opening bracket as few times as possible until closing bracket) and trailing number
            var regex = new Regex(@"\{(?<Before>[^PGFS]*)(?<Identifier>[PGFS]*)(?<After>[^\}]*)\}(?:(?<Skip>\d*?(?=\:))\:)?(?<Take>[\d\*]*)", RegexOptions.IgnoreCase);

            var match = regex.Match(formatPart);

            var identifier = match.Groups["Identifier"].Value;
            var beforeIdentifier = match.Groups["Before"].Value;
            var afterIdentifier = match.Groups["After"].Value;
            var take = match.Groups["Take"].Value;
            var skip = match.Groups["Skip"].Value;

            var caseType = CaseType.Normal;


            // TODO refactor, throw error or smth (maybe log too)
            // if identifier was missing, we can't really format anything, return null
            if (String.IsNullOrEmpty(identifier))
            {
                return null;
            }

            // get part of name to format
            var namePart = getNamePartByFormatString(name, identifier, !String.IsNullOrEmpty(take));

            // split by comma, if namepart contains commas, else by space
            IEnumerable<String> names = null;
            var namePartSeparator = namePart.IndexOf(',') > 0 ?
                new char[] { ',' } :
                new char[] { ' ' };
            names = namePart.Split(namePartSeparator, StringSplitOptions.RemoveEmptyEntries);


            if (!String.IsNullOrWhiteSpace(skip) &&
                skip.ToCharArray().All(c => char.IsDigit(c)))
            {
                // number of names to show
                var amountToSkip = int.Parse(skip);
                names = names.Skip(amountToSkip);
            }

            if (!String.IsNullOrWhiteSpace(take) &&
                take.ToCharArray().All(c => char.IsDigit(c)))
            {
                // number of names to show
                var count = int.Parse(take);
                names = names.Take(count);
            }

            // show only initials if formatstring letter part length is 1
            if (identifier.Length == 1)
            {
                names = names.Select(
                    n => n.IndexOf('-') <= 0 ?
                        n.Substring(0, 1) :
                        String.Join("-",
                        n.Split('-').Select(n2 => n2.Substring(0, 1)))
                    );
            }

            caseType = getCaseType(identifier);
            switch (caseType)
            {
                case CaseType.Lower:
                    names = names.Select(n => n.ToLowerInvariant());
                    break;
                case CaseType.Upper:
                    names = names.Select(n => n.ToUpperInvariant());
                    break;
                case CaseType.Normal:
                default:
                    break;
            }

            names = names.Select(n => String.Format("{0}{1}{2}", beforeIdentifier, n, afterIdentifier));

            return String.Join(" ", names).Trim(' ', ',');
        }

        private static String getNamePartByFormatString(PersonName name, String identifier, bool takeOnlyFullName)
        {
            var letter = identifier.Substring(0, 1).ToUpper();
            var namePart = "";

            switch (letter)
            {
                case "G":
                    // if preferred name exists and we don't want all name elements
                    if (!String.IsNullOrWhiteSpace(name.PreferredName) && !takeOnlyFullName)
                    {
                        namePart = name.PreferredName;
                    }
                    else
                    {
                        namePart = name.GivenName;
                    }
                    break;
                case "F":
                    namePart = name.FamilyName;
                    break;
                case "P":
                    namePart = name.Prefix;
                    break;
                case "S":
                    namePart = name.Suffix;
                    break;
            }

            return namePart;
        }

        private enum CaseType { Upper, Lower, Normal }

        private static CaseType getCaseType(String identifier)
        {
            var chars = identifier.ToCharArray();
            var caseType = CaseType.Normal;

            if (Char.IsLower(chars[0]))
            {
                caseType = CaseType.Lower;
            }
            else if (Char.IsUpper(chars[0]))
            {
                if (chars.Length == 1 || Char.IsUpper(chars[1]))
                {
                    caseType = CaseType.Upper;
                }
            }

            return caseType;
        }
    }
}
