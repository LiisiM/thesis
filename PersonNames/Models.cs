﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonNames
{
    public class PersonName
    {
        public String Prefix { get; set; }
        public String GivenName { get; set; }
        public String FamilyName { get; set; }
        public String Suffix { get; set; }

        public String PreferredName { get; set; }
        public String FullName { get; set; } // original entered full name

        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }

        public NameUse? Use { get; set; }

        public override String ToString()
        {
            return this.ToString("{Gg} {Ff}");
        }

        public String ToString(string format)
        {
            return Formatter.Format(this, format);
        }

        public PersonName Parse(string inputName)
        {
            return Parser.Parse(inputName);
        }
    }

    public enum NameUse { Official, Nickname, Artist }

}
