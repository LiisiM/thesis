﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PersonNames
{
    public static class Parser
    {
        // Chars to always remove from beginning and end of found names
        private static char[] _charsToTrim = { ' ', ',' };

        public static PersonName Parse(String nameString)
        {
            // remove excess whitespace
            nameString = nameString.Trim();

            var name = new PersonName { FullName = nameString };

            List<String> foundPrefixes = null;
            tryRemovePrefixes(nameString, out foundPrefixes, out nameString);
            name.Prefix = String.Join(",", foundPrefixes);

            // remove suffixes
            List<String> foundSuffixes = null;
            tryRemoveSuffixes(nameString, out foundSuffixes, out nameString);
            name.Suffix = String.Join(",", foundSuffixes);

            // remove familyname
            String foundFamilyName = "";
            tryRemoveFamilyName(nameString, out foundFamilyName, out nameString);
            name.FamilyName = foundFamilyName;

            // given name remains
            name.GivenName = nameString;

            return name;
        }

        private static bool tryRemovePrefixes(String nameString, out List<String> foundPrefixes, out String nameStringWithoutPrefixes)
        {
            foundPrefixes = new List<String>();

            var prefixes = PersonNames.Data.Prefixes.Split(',');
            // TODO find a way to get the prefix that string starts with, to remove foreach loop, becuase current complexity is n^2

            // remove all prefixes that match defined ones and put them in list
            while (prefixes.Any(p => nameString.StartsWith(p)))
            {
                foreach (var prefix in prefixes)
                {
                    if (nameString.IndexOf(prefix) == 0)
                    {
                        // if there is . right after prefix, take this too
                        var takeLength = (nameString.IndexOf('.') == prefix.Length ?
                            prefix.Length + 1 :
                            prefix.Length);

                        foundPrefixes.Add(nameString.Substring(0, takeLength)
                            .Trim(_charsToTrim));

                        nameString = nameString
                            .Substring(takeLength)
                            .Trim(_charsToTrim);
                    }
                }
            }

            var nameParts = nameString.Split(' ');
            var names = new Stack<String>(nameParts.Reverse());

            // remove all prefixes that end with .
            while (names.Peek().IndexOf('.') > 0)
            {
                foundPrefixes.Add(names.Pop());
            }

            nameStringWithoutPrefixes = String.Join(" ", names);
            return true;
        }

        // TODO correctly parse suffixes that contain spaces (like "kahe osaline")
        private static bool tryRemoveSuffixes(String nameString, out List<String> foundSuffixes, out String nameStringWithoutSuffixes)
        {
            foundSuffixes = new List<String>();

            // ei saa kõigepealt väljalõigata tuntuid ja siis kõiki ülejäänuid, sest peavad jääma samasse järjekorda
            var suffixStartIndex = nameString.IndexOf(',');
            if (suffixStartIndex >= 0)
            {
                foundSuffixes = nameString.Substring(suffixStartIndex)
                                        .Split(',')
                                        .Where(s => !String.IsNullOrWhiteSpace(s))
                                        .Select(s => s.Trim())
                                        .ToList();

                nameString = nameString
                    .Substring(0, suffixStartIndex)
                    .Trim(_charsToTrim);
            }

            var definedSuffixes = PersonNames.Data.Suffixes.Split(',');
            while (definedSuffixes.Any(s => nameString.EndsWith(s)))
            {
                foreach (var suffix in definedSuffixes)
                {
                    if (nameString.EndsWith(suffix))
                    {
                        var suffixIndex = nameString.LastIndexOf(suffix);
                        foundSuffixes.Insert(0, nameString
                            .Substring(suffixIndex)
                            .Trim(_charsToTrim));

                        nameString = nameString.Substring(0, suffixIndex)
                            .Trim(_charsToTrim);
                    }
                }
            }

            nameStringWithoutSuffixes = nameString;
            return true;
        }

        private static bool tryRemoveFamilyName(String nameString, out String familyName, out String nameStringWithoutFamilyName)
        {
            var nameParts = nameString.Split(' ');
            var names = new Stack<String>(nameParts);

            var familyNameQueue = new Stack<String>();

            // take last as familyname
            familyNameQueue.Push(names.Pop());


            // if next is join word, add this and element before it to family name 
            // for example, join word = und, add 'und' and the name element in front of it
            var familyNameJoins = PersonNames.Data.FamilyNameJoins.Split(',');
            if (names.Count > 0 &&
                familyNameJoins.Any(j => j.Equals(names.Peek(),
                    StringComparison.InvariantCultureIgnoreCase)))
            {
                familyNameQueue.Push(names.Pop());
                familyNameQueue.Push(names.Pop());
            }

            // check for nobility particles before familyname, if any exists, take them too
            var nobilityPrefixes = PersonNames.Data.NobilityParticles.Split(',');
            while (names.Count > 0 &&
                nobilityPrefixes.Any(n => n.Equals(names.Peek(),
                    StringComparison.InvariantCultureIgnoreCase)))
            {
                familyNameQueue.Push(names.Pop());
            }

            // also check for nobility title in name
            var wordsInFamilyName = PersonNames.Data.DefinedWordsInFamilyName.Split(',');
            if (names.Count > 0 &&
                wordsInFamilyName.Any(t => t.Equals(names.Peek(),
                    StringComparison.InvariantCultureIgnoreCase)))
            {
                familyNameQueue.Push(names.Pop());
            }

            familyName = String.Join(" ", familyNameQueue);
            // everything that remains are given names (reverse, because they are in reversed order in stack)
            nameStringWithoutFamilyName = String.Join(" ", names.Reverse());

            return true;
        }
    }
}
