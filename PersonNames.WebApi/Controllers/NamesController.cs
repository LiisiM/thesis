﻿using System;
using System.Web.Http;
using log4net;
using PersonNames.WebApi.Models;

namespace PersonNames.WebApi.Controllers
{
    [RoutePrefix("api/Names")]
    public class NamesController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger
    (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpGet]
        [Route("Format")]
        public IHttpActionResult Format(String format, String fullName, String preferredName = "")
        {
            log.Info("Log is working");

            var name = Parser.Parse(fullName.Trim());
            //if (preferredName != "") { name.PreferredName = preferredName; }
            name.PreferredName = preferredName;
            var formattedName = name.ToString(format);

            var response = new FormatResponse
            {
                Format = format,
                FormattedName = formattedName,
                FullName = fullName,
                GivenName = name.GivenName,
                FamilyName = name.FamilyName,
                Prefix = name.Prefix,
                Suffix = name.Suffix,
                PreferredName = name.PreferredName
            };

            return Ok(response);
        }

        [HttpGet]
        [Route("Format")]
        public IHttpActionResult Format(String format, String givenName = "", String familyName = "", String prefix = "", String suffix = "")
        {
            var name = new PersonName
            {
                GivenName = givenName,
                FamilyName = familyName,
                Prefix = prefix,
                Suffix = suffix
            };

            var formatted = name.ToString(format);

            return Ok(formatted);
        }
    }
}
