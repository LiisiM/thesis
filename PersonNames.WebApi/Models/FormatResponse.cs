﻿using System;

namespace PersonNames.WebApi.Models
{
    public class FormatResponse
    {
        public String FullName { get; set; }
        public String GivenName { get; set; }
        public String FamilyName { get; set; }
        public String Suffix { get; set; }
        public String Prefix { get; set; }
        public String PreferredName { get; set; }
        public String Format { get; set; }
        public String FormattedName { get; set; }
    }
}