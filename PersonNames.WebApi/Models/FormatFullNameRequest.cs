﻿using System;

namespace PersonNames.WebApi.Models
{
    public class FormatFullNameRequest
    {
        //public FormatFullNameRequest()
        //{
        //    PreferredName = "";
        //}

        public String Format { get; set; }
        public String FullName { get; set; }
        public String PreferredName { get; set; }

        //public String ToString()
        //{
        //    return String.Format("Format: {0}\nFullName: {1}\nPreferredName: {2}", 
        //        this.Format,
        //        this.FullName, 
        //        this.PreferredName);
        //}
    }
}