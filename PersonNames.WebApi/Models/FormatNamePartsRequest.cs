﻿using System;

namespace PersonNames.WebApi.Models
{
    public class FormatNamePartsRequest
    {
        public FormatNamePartsRequest()
        {    
            GivenName = "";
            FamilyName = "";
            Prefix = "";
            Suffix = "";
            PreferredName = "";
        }

        public String Format { get; set; }
        public String GivenName { get; set; }
        public String FamilyName { get; set; }
        public String Prefix { get; set; }
        public String Suffix { get; set; }
        public String PreferredName { get; set; }

        //public String ToString()
        //{
        //    return String.Format("Format: {0}\nGivenName: {1}\nFamilyName: {2}\nPrefix: {3}\nSuffix: {4}\nPreferredName: {5}", 
        //        this.Format, 
        //        this.GivenName, 
        //        this.FamilyName, 
        //        this.Prefix, 
        //        this.Suffix,
        //        this.PreferredName);
        //}
    }
}