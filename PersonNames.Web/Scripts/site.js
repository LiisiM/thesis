﻿var r = {};
(function () {
    $(document).ready(function () {
        $('form#demo').on('submit', function (e) {
            e.preventDefault();

            $.ajax({
                url: 'http://localhost:49354/api/Names/Format',
                data: $(this).serialize()
            }).done(function (data) {
                r = data;
                console.log(data);

                $('#formatted').text(data.formattedName);

                var html = '<table class="table"><thead><tr><th>Key</th><th>Value</th></tr></thead><tbody>';
                $.each(r, function (k, v) {
                    html += '<tr><td class="key">' + k + '</td>';
                    html += '<td class="value">' + v + '</td></tr>';
                });
                html += '</tbody></table>';
                $('#result').html(html);
            }).fail(function (data) {
                console.log('error', data);
            });
        });
    });
})();